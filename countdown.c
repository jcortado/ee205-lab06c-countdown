///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 59
//   Years: 7 Days: 12 Hours: 6 Minutes: 21 Seconds: 0
//
// @author Jordan Cortado <jcortado@hawaii.edu>
// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <time.h>
#include <unistd.h>

int main() {

   int secondsPerMin = 60;
   int secondsPerHour = secondsPerMin * 60;
   int secondsPerDay = secondsPerHour * 24;
   int secondsPerYear = secondsPerDay * 365;

   struct tm myBday;
   
   time_t currentTime;
   time_t refTime;
   double currentRefDiff;

   int numYear; int numDay; int numHour; int numMin; int numSec;

   myBday.tm_year = 1999-1900;
   myBday.tm_mon = 1;
   myBday.tm_mday = 12;
   myBday.tm_wday = 5;
   myBday.tm_hour = 22;
   myBday.tm_min = 10;
   myBday.tm_sec = 1;
   myBday.tm_isdst = -1;

   refTime = mktime(&myBday);

   char buffer[100];
   strftime( buffer, 100, "%a %b %d %X %p %Z %Y", &myBday);
   printf("Reference time: %s\n", buffer);

   while( true ){

      currentTime = time(NULL);

      currentRefDiff = difftime( currentTime, refTime);

      numYear = currentRefDiff / secondsPerYear;
      currentRefDiff = (int)currentRefDiff % secondsPerYear;

      numDay = currentRefDiff / secondsPerDay;
      currentRefDiff = (int)currentRefDiff % secondsPerDay;

      numHour = currentRefDiff / secondsPerHour;
      currentRefDiff = (int)currentRefDiff % secondsPerHour;

      numMin = currentRefDiff / secondsPerMin;
      currentRefDiff = (int)currentRefDiff % secondsPerMin;

      numSec = currentRefDiff;

      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d \n", numYear, numDay, numHour, numMin, numSec);
      
      sleep(1);
   }

   return EXIT_SUCCESS;
}
